window.addEventListener('DOMContentLoaded', async () => {

    const url = "http://localhost:8000/api/locations/"; // define the URL where we'll fetch data for the list of LOCATIONS
    
    const response = await fetch(url); // fetch the data from the URL
    
    if (response.ok) {
        const data = await response.json(); // parse the JSON response to a JavaScript object

        const selectTag = document.getElementById('location'); // get the select element where we'll populate the LOCATIONS
        if (Array.isArray(data.locations)) {
            for (let location of data.locations) { // loop through the list of LOCATIONS to create option elements for each
                const option = document.createElement('option');
                option.value = location.id; // set the value of the option to be the location's ID
                option.innerHTML = location.name; // set the display text to be the location's name
                selectTag.appendChild(option); // append the new option to the select element
            };
        } else {
            console.log("Unexpected data format, data.locations is not an array");
        }
    } else {
        const errorData = await response.json();
        console.error("Fetch error:", errorData);
        console.log("problem with locations fetching")
    }
    // GETTING THE FORM DATA
    const formTag = document.getElementById('create-conference-form'); // select the form element by its ID
    formTag.addEventListener('submit', async event => { // add an event listener for the 'submit' event
        event.preventDefault(); // the event handler should do the preventDefault call. Prevent the default form submission action (page reload)
        const formData = new FormData(formTag); // Create a FormData object from the form element
        const json = JSON.stringify(Object.fromEntries(formData)); // Convert the FormData to a JSON string
        console.log(json);
        
        // API REQUEST: SENDING THE DATA TO THE SERVER
        // Define the URL and configuration for the POST request to create a new CONFERENCE
        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "POST", 
            body: json,
            headers: {
                'Content-Type': 'application/json', 
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig); // make the POST request to create the new CONFERENCE
        if (response.ok) { // if the POST was successful, 
            formTag.reset(); // reset the form
            const newConference = await response.json(); // Parse the JSON response to get the new CONFERENCE data
            console.log(newConference);
        } else {
            const errorData = await response.json();
            console.error("Fetch error:", errorData);
            console.log("problem with conference POST API call")
        }
    });
    
});