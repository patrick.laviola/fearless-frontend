window.addEventListener('DOMContentLoaded', async () => {

    const url = "http://localhost:8000/api/states/"; // define the URL where we'll fetch data for the lsit of states
    
    const response = await fetch(url); // fetch the data from the URL
    
    if (response.ok) {
        const data = await response.json(); // parse the JSON response to a JavaScript object
        console.log(data);

        const selectTag = document.getElementById('state'); // get the select element where we'll populate the states
        for (let state of data.states) { // loop through the list of states to create option elements for each
            const option = document.createElement('option');
            option.value = state.abbreviation; // set the value of the option to be the state's abbreviation
            option.innerHTML = state.name; // set the display text to be the state's name
            selectTag.appendChild(option); // append the new option to the select element
        }
    }
    // GETTING THE FORM DATA
    const formTag = document.getElementById('create-location-form'); // select the form element by its ID
    formTag.addEventListener('submit', async event => { // add an event listener for the 'submit' event
        event.preventDefault(); // the event handler should do the preventDefault call. Prevent the default form submission action (page reload)
        const formData = new FormData(formTag); // Create a FormData object from the form element
        const json = JSON.stringify(Object.fromEntries(formData)); // Convert the FormData to a JSON string
        
        // API REQUEST: SENDING THE DATA TO THE SERVER
        // Define the URL and configuration for the POST request to create a new location
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "POST", // In JavaScript's 'fetch' API, the 'method' property for the configuration object is case-insensitive
            body: json,
            headers: { // headers property is used to include any HTTP headers that you want to send along with your request. They provide meta-information about the request or response, or about the object sent in the message body.
                'Content-Type': 'application/json', // this tells the server that the body of the request contains a JSON-formatted object.The server uses this information to properly parse the body content. This is impostant because the server needs to know in what format the incoming data is, in order to handle it appropriately. Remember that headers are key-value pairs, and you can include more headers depending on your use-case. For example if your API requires authentication, you might include an 'Authorization' header like this:
                // headers: {
                //     'Content-Type': 'application/json',
                //     'Authorization': 'Bearer ' + yourAccessToken
                // }
            },
        };

        const response = await fetch(locationUrl, fetchConfig); // make the POST request to create the new location
        if (response.ok) { // if the POST was successful, 
            formTag.reset(); // reset the form
            const newLocation = await response.json(); // Parse the JSON response to get the new location data
            // console.log(newLocation);
        }
    });
    
});