function createCard(name, description, pictureUrl, start, end, location) {
    return `
        <div class="card shadow p-3 mb-4 bg-body-tertiary rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-body-secondary text-secondary">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                ${start} - ${end}
            </div>
        </div>
    `;
}

function formatDate(dateString) {
    const options = { year: 'numeric', month: '2-digit', day: '2-digit' };
    return new Date(dateString).toLocaleDateString(undefined, options);
}

function createAlert(message) {
    return `
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            ${message}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    `;
}

function createPlaceholderCard() {
    return `
        <div class="card shadow p-3 mb-4 bg-body-tertiary rounded placeholder-card">
            <div class="card-body">
                <div class="placeholder-glow">
                    <span class="placeholder placeholder-lg w-50"></span>
                    <span class="placeholder placeholder-sm"></span>
                    <span class="placeholder placeholder-md"></span>
                </div>
            </div>
        </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error('Response not ok');
        } else {
            const data = await response.json();
            
            let colIndex = 0;
            
            for (let conference of data.conferences) {
                const columnId = `col${(colIndex % 3) + 1}`;
                const column = document.getElementById(columnId);
                column.innerHTML += createPlaceholderCard();

                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = formatDate(details.conference.starts);
                    const endDate = formatDate(details.conference.ends);
                    const location = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, startDate, endDate, location);

                    // const columnId = `col${(colIndex % 3) + 1}`;
                    // const column = document.getElementById(columnId);
                    // column.innerHTML += html;

                    const placeholderCard = column.querySelector('.placeholder-card');
                    if (placeholderCard) {
                        placeholderCard.outerHTML = html;
                    }

                    colIndex++;
                } else {
                    const placeholderCard = column.querySelector('.placeholder-card');
                    if (placeholderCard) {
                        placeholderCard.remove();
                    }
                }
            }
        }
    } catch (e) {
        console.error('error', e);
        const alertMessage = "An error occurred while fetching data. Please try again later.";
        const alertHtml = createAlert(alertMessage);
        document.body.insertAdjacentHTML('afterbegin', alertHtml);
        // const alertDiv = document.createElement('div');
        // alertDiv.className = 'alert alert-danger alert-dismissible fade show';
        // alertDiv.role = 'alert';
        // alertDiv.innerText = 'An error occurred while fetching data';
        // document.body.prepend(alertDiv);
    }
});